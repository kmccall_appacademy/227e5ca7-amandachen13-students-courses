class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_reader :first_name, :last_name, :courses

  def name
    first_name + " " + last_name
  end

  def enroll(new_course)
    courses.each do |course|
      if course.conflicts_with?(new_course)
        raise "course conflicts with already enrolled course"
      end
    end

    new_course.students << self
    unless @courses.include?(new_course)
      @courses.push(new_course)
    end
  end

  def course_load
    course_credits = Hash.new(0)
    @courses.each do |course|
      course_credits[course.department] += course.credits
    end
    course_credits
  end

end
